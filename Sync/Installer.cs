﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Configuration.Install;
using System.Linq;
using System.Threading.Tasks;
using System.ServiceProcess;

namespace Watcher
{
    /// <summary>
    /// This enables InstallUtil.exe to set up the windows service.
    /// </summary>
    [RunInstaller(true)]
    public class WatcherInstaller : Installer {

        /****************************************************************/
        #region Class Member Definition

        /****************************************************************/
        #region Private Member Variables

        /// <summary>
        /// Holds the service process installer for the duration of the setup.
        /// </summary>
        private ServiceProcessInstaller m_process;

        /// <summary>
        /// Holds the service installer for the duration of the setup.
        /// </summary>
        private ServiceInstaller m_service;

        #endregion

        #endregion

        /****************************************************************/
        #region Construction and Finalisation

        /// <summary>
        /// Default constructor for the service installer
        /// </summary>
		public WatcherInstaller()
		{

			// if built in debug mode, then attach a debugger to the service.
			#if DEBUG

				// launches and attaches the debugger. 
				// System.Diagnostics.Debugger.Launch();

			#endif

            // creates the service process installer
            this.m_process = new ServiceProcessInstaller();

            // sets the account to be used with the service (local system should have access to everything).
            this.m_process.Account = ServiceAccount.LocalSystem;

            // creates the service installer.
            this.m_service = new ServiceInstaller();

            // sets the service name for the new service.
			// this.m_service.ServiceName = "Watcher"; // this should be specified on the command line to facilitate multiple instances

            // sets the startup type for the service to automatic.
            this.m_service.StartType = ServiceStartMode.Automatic;

            // adds the service process installer and the service installer to the collection of installers.
            this.Installers.Add(m_process);
            this.Installers.Add(m_service);
        }

        #endregion

		/// <summary>
		/// allows service name to be specified on command line when installing
		/// </summary>
		private void RetrieveServiceName()
		{
			var serviceName = Context.Parameters["servicename"];
			if (!string.IsNullOrEmpty(serviceName))
			{
				this.m_service.ServiceName = serviceName;
				this.m_service.DisplayName = serviceName;
			}
		}

		public override void Install(System.Collections.IDictionary stateSaver)
		{
			RetrieveServiceName();
			base.Install(stateSaver);
		}

		public override void Uninstall(System.Collections.IDictionary savedState)
		{
			RetrieveServiceName();
			base.Uninstall(savedState);
		}
    }
}
