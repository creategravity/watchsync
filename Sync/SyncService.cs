﻿using FluentFTP;
using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Net;
using System.Reflection;
using System.Security.Permissions;

namespace Sync
{
	class SyncService
	{
		// Create a new FileSystemWatcher and set its properties.
		private static FileSystemWatcher watcher = new FileSystemWatcher();
		
		private string local = null;
		private string remote = "/";
		private string filter = "*.*";
		private string eventSource = null;
		private string host = null;
		private string user = null;
		private string pass = null;
		private FtpClient client = null;
		
		// hashset contains a list of file that have to be sent to remote server
		private HashSet<string> files = new HashSet<string>();

		[PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
		public SyncService()
		{

			// this is the path to watch
			local = SyncUtils.GetConfigValue("local");

			// remote folder to upload to
			remote = SyncUtils.GetConfigValue("remote");

			// this is the file filter to watch for
			filter = SyncUtils.GetConfigValue("filter");

			host = SyncUtils.GetConfigValue("host");
			user = SyncUtils.GetConfigValue("user");
			pass = SyncUtils.GetConfigValue("pass");

			// name of this instance
			eventSource = SyncUtils.GetConfigValue("name");
			SyncUtils.SetEventSource(eventSource);

			// If a directory is not specified, exit program.
			if (!Directory.Exists(local))
			{
				throw new DirectoryNotFoundException("Specified local path cannot be found");
			}
			
			watcher.Path = local;

			// Watch for changes in LastAccess and LastWrite times, and the renaming of files or directories.
			// watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
			watcher.NotifyFilter = NotifyFilters.LastWrite;

			// watch all files.
			watcher.Filter = filter;

			watcher.IncludeSubdirectories = true;

			// Add event handlers.
			watcher.Changed += new FileSystemEventHandler(OnChanged);
			watcher.Created += new FileSystemEventHandler(OnChanged);
			// watcher.Deleted += new FileSystemEventHandler(OnChanged);
			// watcher.Renamed += new RenamedEventHandler(OnRenamed);

			// Begin watching.
			watcher.EnableRaisingEvents = true;
			
			// create an FTP client
			client = new FtpClient(host);
			
			// if you don't specify login credentials, we use the "anonymous" user account
			client.Credentials = new NetworkCredential(user, pass);

			// begin connecting to the server
			client.Connect();

			SyncUtils.Log("Sync initialised");
		}


		// Define the event handlers.
		private void OnChanged(object source, FileSystemEventArgs e)
		{
			// adding files to the hash set ensures that they only get added once
			// to add them again, they'll have to be removed, which should be done only after
			// they have been successfully uploaded
			Add(e.FullPath);
			// SyncUtils.Log(String.Format("Added: {0}", e.FullPath));

			// Specify what is done when a file is changed, created, or deleted.
			// Log(String.Format("{0}: {1}", e.ChangeType, e.FullPath));

		}

		public void Add(string file)
		{
			// ensures that only one thread can access this collection, blocking others forcing them to wait
			lock (files) files.Add(file);
		}

		public void Remove(string file)
		{
			// ensures that only one thread can access this collection, blocking others forcing them to wait
			lock (files) files.Remove(file);
		}

		public void CheckFilesToSync()
		{
			// SyncUtils.Log("CheckFilesToSync called");
			if (client != null)
			{
				if (files.Any())
				{
					// we can't modify the set that we're working with as we're iterating over it
					// so we create a copy using ToArray
					foreach (string filename in files.ToArray())
					{
						string remoteFilename = SyncUtils.TranslateLocalPathToRemote(filename, local, remote);

						if (!String.IsNullOrWhiteSpace(remoteFilename))
						{
							// SyncUtils.Log(String.Format("About to Upload: {0} >>> {1}", filename, remoteFilename));

							// if there are spaces in the filename, it might cause issues with the path, better to not have spaces!!
							// the current ftp client FluentFTP seemingly doesn't like spaces
							if (!remoteFilename.Contains(" "))
							{
								using (var stream = File.Open(filename, FileMode.Open, FileAccess.Read, FileShare.Read))
								{
									bool status = client.Upload(stream, remoteFilename, FtpExists.NoCheck, true);
									if (status)
									{
										SyncUtils.Log(String.Format("Successfully Uploaded: {0} >>> {1}", filename, remoteFilename));
									}
									else
									{
										SyncUtils.Log(String.Format("Could not Upload: {0} >>> {1}", filename, remoteFilename), EventLogEntryType.Warning);
									}
								}

							}
							else
							{
								SyncUtils.Log(String.Format("Could not Upload file, space in path: {0} >>> {1}", filename, remoteFilename), EventLogEntryType.Warning);
							}

							// removes the file from the list of sync files, if the same file is modified later, it will be put back into the list 
							// by the file watcher process automatically
							Remove(filename);
						}
						else
						{
							// SyncUtils.Log("filename empty");
						}
					}
				}
				else
				{
					// SyncUtils.Log("No files to sync");
				}
			}
			else
			{
				// SyncUtils.Log("No client available");
			}
		}

		private void OnRenamed(object source, RenamedEventArgs e)
		{
			// Specify what is done when a file is renamed.
			SyncUtils.Log(String.Format("File: {0} renamed to {1}", e.OldFullPath, e.FullPath));
		}

		public void Dispose()
		{
			if (client != null)
			{
				client.Disconnect();

				SyncUtils.Log("Sync stopped");
			}
		}
	}

	class SyncUtils
	{
		private static string m_eventSource = "Sync";

		public static void SetEventSource(string name)
		{
			m_eventSource = name;
		}

		public static bool Log(string message, EventLogEntryType entryType = EventLogEntryType.Information)
		{
			if (!EventLog.SourceExists(m_eventSource))
			{
				throw new Exception("EventLog source could not be found");
			}

			EventLog.WriteEntry(m_eventSource, message, entryType);

			return true;
		}
		public static string GetConfigValue(string appConfigKey)
		{
			// get the app.config (renamed to <assemblyname>.exe.config when compiled
			Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);

			return config.AppSettings.Settings[appConfigKey].Value;
		}


		public static string TranslateLocalPathToRemote(string fullLocalPath, string localRoot, string remoteRoot)
		{
			string ret = "";

			// full path
			// C:\inetpub\domains\creategravity.com\downloads\terry\image.png

			// remote path
			// /public_html/downloads/terry/image.png

			// local root
			// C:\inetpub\domains\creategravity.com\downloads\terry

			// remote root
			// /public_html/terry

			if (!localRoot.EndsWith("\\"))
			{
				// adds backslash to end of string
				localRoot += "\\";
			}

			int pos = fullLocalPath.IndexOf(localRoot);

			// should change string to image.png
			ret = fullLocalPath.Substring(localRoot.Length);

			if (!remoteRoot.EndsWith("/"))
			{
				// adds backslash to end of string
				remoteRoot += "/";
			}

			// tack it onto the end of the remote root
			ret = remoteRoot + ret.Replace("\\", "/");

			return ret;
		}
	}
}
