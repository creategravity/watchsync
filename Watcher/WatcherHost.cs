﻿
using System.Configuration;
using System.IO;
using System.Reflection;
using System.ServiceModel;
using System.ServiceProcess;


namespace Watcher
{
    
    /// <summary>
    /// Class used to start the service hosts.
    /// </summary>
    class WatcherHost : ServiceBase {

        #region Class Member Definition

        #region Public Properties

        /// <summary>
        /// Holds the service host for the List Smart Web service.
        /// </summary>
        public ServiceHost ServiceHost = null;

        #endregion

        #endregion

        #region Construction and Finalisation

        /// <summary>
        /// Default constuctor
        /// </summary>
		public WatcherHost()
		{
			var ws = new WatcherService();
        }

        #endregion

        #region Public Static Methods

        /// <summary>
        /// Entry point for the application - runs the service as a new windows service.
        /// </summary>
        public static void Main() {

            // Instantiates a new service to run as a windows service.
			ServiceBase.Run(new WatcherHost());
        }

        /// <summary>
        /// Gets a value from the app.config - workaround for not loaded issue.
        /// </summary>
        /// <param name="appConfigKey">the app config key used for retrieving the value.</param>
        /// <returns>the value against the key supplied.</returns>
        public static string GetConfigValue(string appConfigKey) {

			// get the app.config (renamed to <assemblyname>.exe.config when compiled
			Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);

			return config.AppSettings.Settings[appConfigKey].Value;
		}

        #endregion

        #region Protected Methods

        /// <summary>
        /// Start the Windows service.
        /// </summary>
        /// <param name="args">arguments passed into the service start</param>
        protected override void OnStart(string[] args) {

            // if built in debug mode, then attach a debugger to the service.
            #if DEBUG

                // launches and attaches the debugger. 
                System.Diagnostics.Debugger.Launch();

            #endif

            // if the web service host is not null, then close it.
            if (this.ServiceHost != null) {

                // closes the service host.
                this.ServiceHost.Close();
            }

            // Create a ServiceHost for the Service
            this.ServiceHost = new ServiceHost(typeof(WatcherService));

            // Open the ServiceHostWeb to create listeners and start listening for messages.
            this.ServiceHost.Open();            
        }

        /// <summary>
        /// Fired when the service is stopped.
        /// </summary>
        protected override void OnStop() {

            // if the service host web is not null.
            if (this.ServiceHost != null) {

                // close the service host.
                this.ServiceHost.Close();

                // null the reference.
                this.ServiceHost = null;
            }
        }

        #endregion
    }
}
