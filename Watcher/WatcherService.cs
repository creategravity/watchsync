﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Diagnostics;
using System.IO;
using System.Linq;
using System.Reflection;
using System.Security.Permissions;
using System.Text;
using System.Threading.Tasks;
using System.Timers;

namespace Watcher
{
	class WatcherService
	{
		public bool Log(string message, EventLogEntryType entryType = EventLogEntryType.Information)
		{
			string sSource = GetConfigValue("name");

			if (!EventLog.SourceExists(sSource))
			{
				throw new Exception("EventLog source could not be found");
			}

			EventLog.WriteEntry(sSource, message, entryType);

			return true;
		}
		public string GetConfigValue(string appConfigKey)
		{
			// get the app.config (renamed to <assemblyname>.exe.config when compiled
			Configuration config = ConfigurationManager.OpenExeConfiguration(Assembly.GetExecutingAssembly().Location);

			return config.AppSettings.Settings[appConfigKey].Value;
		}



		// Create a new FileSystemWatcher and set its properties.
		public static FileSystemWatcher watcher = new FileSystemWatcher();

		private DateTime m_detectedChangeDate = DateTime.MinValue;

		private string path = null;
		private string command = null;
		private int timeout = 60;
		private string filter = "*.*";
		private string name = null;

		Timer aTimer = null;


		[PermissionSet(SecurityAction.Demand, Name = "FullTrust")]
		public WatcherService()
		{
			// this is the path to watch
			path = GetConfigValue("path");

			// this is the command to run
			command = GetConfigValue("command");

			// this is the time to wait before running the command
			timeout = Convert.ToInt32(GetConfigValue("timeout"));

			// this is the file filter to watch for
			filter = GetConfigValue("filter");

			// name of this instance
			name = GetConfigValue("name");

			// If a directory is not specified, exit program.
			if (!Directory.Exists(path))
			{
				throw new DirectoryNotFoundException("Specified path cannot be found");
			}

			// If a directory is not specified, exit program.
			if (!File.Exists(command))
			{
				throw new FileNotFoundException("Specified command cannot be found");
			}


			watcher.Path = path;
			/* Watch for changes in LastAccess and LastWrite times, and the renaming of files or directories. */
			watcher.NotifyFilter = NotifyFilters.LastAccess | NotifyFilters.LastWrite | NotifyFilters.FileName | NotifyFilters.DirectoryName;
			// watch all files.
			watcher.Filter = filter;
			watcher.IncludeSubdirectories = true;

			// Add event handlers.
			watcher.Changed += new FileSystemEventHandler(OnChanged);
			watcher.Created += new FileSystemEventHandler(OnChanged);
			watcher.Deleted += new FileSystemEventHandler(OnChanged);
			watcher.Renamed += new RenamedEventHandler(OnRenamed);

			// Begin watching.
			watcher.EnableRaisingEvents = true;

			// set the timer
			aTimer = new Timer(1000 * timeout);
			aTimer.Elapsed += new ElapsedEventHandler(OnTimedEvent);

			// the time will be enabled once a file change has been detected
			aTimer.Enabled = false;

			Log("Watcher initialised");
		}

		// Define the event handlers.
		private void OnChanged(object source, FileSystemEventArgs e)
		{
			// Specify what is done when a file is changed, created, or deleted.
			// Console.WriteLine("File: " + e.FullPath + " " + e.ChangeType);

			DetectedChange();
		}

		private void OnRenamed(object source, RenamedEventArgs e)
		{
			// Specify what is done when a file is renamed.
			// Console.WriteLine("File: {0} renamed to {1}", e.OldFullPath, e.FullPath);

			DetectedChange();
		}

		private void DetectedChange()
		{
			Log("DetectedChange");
			if ((DateTime.Now - m_detectedChangeDate).TotalSeconds > timeout)
			{
				// sets the command to run once the timeout has elapsed
				aTimer.Enabled = true; 

				// this sets the m_detectedChangeDate once every timeout period
				// allows time for files to be copied
				// database connections to be closed etc
				m_detectedChangeDate = DateTime.Now;

				Log("Timer reset, timed event scheduled");
			}
			else
			{
				Log("Change ignored, timer already set");
			}
		}

		private void OnTimedEvent(object source, EventArgs e)
		{
			Log("OnTimedEvent");
			if (aTimer.Enabled)
			{
				Log("Timer disabled");

				aTimer.Enabled = false; // only allow this to run once


				Log("command initiated");
				// start the command after the timeout
				System.Diagnostics.Process.Start(command);
			}
			else
			{
				Log("command not called, timer not enabled");
			}
		}
	}
}
